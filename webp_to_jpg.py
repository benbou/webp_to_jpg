import os
from PIL import Image


def main():
    set_cwd()
    webp_files = find_webp_files()
    convert_webp_files(webp_files)


def set_cwd():
    print("Please enter the system path that contains webp files:")
    path = input()
    try:
        os.chdir(path)
    except FileNotFoundError as e:
        print(f"The path {path} does not exist.")
        raise e
    except NotADirectoryError as e:
        print(f"The path {path} is not a directory.")
        raise e
    except PermissionError as e:
        print(f"You do not have permission to access {path}.")
        raise e
    except Exception as e:
        print(f"An error occurred: {e}")
        raise e


def find_webp_files():
    print("Searching for WebP files in the current directory...")
    webp_files = [file for file in os.listdir() if file.endswith(".webp")]
    print(f"Found {len(webp_files)} WebP file(s).")
    return webp_files


def convert_webp_files(webp_files):
    for file in webp_files:
        try:
            print(f"Processing {file}...")
            # Open the WebP image file
            with Image.open(file) as im:
                # Convert to JPG format
                im.convert("RGB").save(os.path.splitext(file)[0] + ".jpg")
            print(f"Conversion successful. Deleting {file}...")
            # Delete the input file after successful conversion
            os.remove(file)
        except AttributeError as e:
            print(f"Error processing {file}: {e}")
        except Exception as e:
            print(f"Error saving {os.path.splitext(file)[0]}.jpg: {e}")
    print("Finished processing all WebP files.")


if __name__ == "__main__":
    main()
