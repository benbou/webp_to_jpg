# WebP to JPG Converter

This Python script converts all WebP image files in a directory to JPG format, and deletes the original WebP files. It requires Python 3.6 or higher and uses the Pillow library to handle image conversion.

## Requirements

To use this script, you will need to have Python 3.6+ installed on your machine, as well as the Pillow library. You can install Pillow using `pip` by running the following command:

```
pip install Pillow
```


## Usage

1. Download the `webp_to_jpg.py` script and save it to your local machine.
2. Open a terminal or command prompt and navigate to the directory containing the `webp_to_jpg.py` script.
3. Run the script by typing `python webp_to_jpg.py` and pressing enter.
4. You will be prompted to enter the system path that contains the WebP files you want to convert. Enter the path and press enter.
5. The script will search for all WebP files in the specified directory, convert them to JPG format, and delete the original WebP files. During the conversion process, the script will print messages to inform you of the current status, including the number of WebP files found, which files are being processed, and any errors that occur during the process.
6. When the script has finished processing all WebP files, a final message will be printed to indicate that the process is complete.

Note that this script will permanently delete the original WebP files after they have been successfully converted to JPG format. Make sure you have a backup of your original files before running this script.

## License

This script is released under the MIT license. See the `LICENSE.txt` file for more details.
